# Unity Client MSI Silent Installation Properties
* SERVICE_LOCATION_DISPLAY_NAME=""
* SERVICE_LOCATION_DATA_SOURCE=""
* SERVICE_LOCATION_NT_AUTH=""
* SERVICE_LOCATION_SERVICE_PATH=""
* UNITYCLIENT_DEFAULT_MAIL_CLIENT=""
* UNITYCLIENT_POP_OPTION=""
* UNITYCLIENT_DOCPOP_URL=""
* CREATE_DESKTOP_SHORTCUTS=""
* CREATE_MENU_SHORTCUTS=""

## For AE
* ADDLOCAL=ApplicationEnabler
* AE_DEFAULTFILE=""
* UNITYCLIENT_AE_HTTPAUTOMATION=""
* UNITYCLIENT_AE_HTTPSAUTOMATION=""
* UNITYCLIENT_AE_PORT=""
* UNITYCLIENT_AE_HTTPS_PORT=""