#!/usr/bin/env python3
from typing import List

from generators import UnityGenerator, ThickClientGenerator
from generators.util import ci_regex


def main(module, stringify: bool, features: List[str]):
    gen = None
    if ci_regex(r"^unity$", module):
        gen = UnityGenerator()
    elif ci_regex(r"^thick$", module):
        gen = ThickClientGenerator()
    else:
        raise NameError(f"Unsupported module: {module}")
    gen.parse_features(features)
    if stringify:
        print(str(gen))
    else:
        with open(f"{module}.yml", "w") as config_file:
            gen.write(config_file)

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("module",
                        help="Which OnBase client are you going to install?",
                        choices=['unity', 'thick', 'app_server'])
    parser.add_argument("--stringify",
                        action='store_true',
                        help="generate the configuration as a string to stdout")
    parser.add_argument("-f", "--features", nargs="*")
    args = parser.parse_args()
    main(args.module, args.stringify, args.features)
