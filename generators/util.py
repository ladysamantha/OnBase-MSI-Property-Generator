def ci_regex(pattern, string):
    import re
    return re.match(pattern, string, flags=re.I)
