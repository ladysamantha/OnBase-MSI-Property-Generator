import re
from typing import Tuple, List

from .base import BaseMsiGenerator

class UnityGenerator(BaseMsiGenerator):
    _feature_regexes: List[Tuple[str, str]] = [
        (r"appserver", "SERVICE_LOCATION_SERVICE_PATH"),
        (r"display_name", "SERVICE_LOCATION_SERVICE_PATH"),
        (r"data_source", "SERVICE_LOCATION_DATA_SOURCE"),
        (r"nt_auth", "SERVICE_LOCATION_NT_AUTH"),
        (r"desktop_shortcuts", "CREATE_DESKTOP_SHORTCUTS"),
        (r"menu_shortcuts", "CREATE_MENU_SHORTCUTS"),
        (r"app_enabler", "ADDLOCAL")
    ]

    def _add_feature(self, parsed_feature: Tuple[str, str]):
        (key, value) = parsed_feature
        feature = [t[1] for t in self._feature_regexes if re.search(t[0], key, flags=re.I)]
        if feature:
            self.config[feature[0]] = value

    def parse_features(self, features: List[str]):
        for feature in features:
            if "=" in feature:
                parsed = self._parse_feature_definition(feature)
                if parsed is None:
                    raise ValueError(f"Feature '{feature}' not of correct format")
                else:
                    self._add_feature(parsed)
            elif re.search(r"app_enabler", feature, flags=re.I):
                self._add_feature(("app_enabler", "ApplicationEnabler"))
            else:
                self._add_feature((feature, "true"))
