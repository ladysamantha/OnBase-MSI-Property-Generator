from typing import List, Tuple

import yaml

from .util import ci_regex

class BaseMsiGenerator(object):
    def __init__(self):
        self.config = dict()

    def write(self, stream):
        yaml.dump(self.config, stream, default_flow_style=False)

    def parse_features(self, features: List[str]):
        raise NotImplementedError(self.parse_features.__name__)

    def _parse_feature_definition(self, feature: str) -> Tuple[str, str]:
        match = ci_regex(r"^(.+)=(.+)$", feature)
        if match:
            return match.group(1, 2)
        else:
            return None

    def __str__(self):
        return ' '.join([f'{feature[0]}="{feature[1]}"' for feature in self.config.items()])
